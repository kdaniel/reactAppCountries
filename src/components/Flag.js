import React from 'react'

const Flag = ( {flag} ) => <img src={flag} alt='missing' width="200" height="200"/>

export default Flag