import React from 'react'
import Languages from './Languages';
import Flag from './Flag';

const Country = ({ country }) => {
  return (
    <>
      <h1> {country.name}</h1>
      <p>capital: {country.capital}</p>
      <p>population: {country.population}</p>

      <h2>Languages</h2>
      
      <Languages languages={country.languages}/>
      <br/>
      <Flag flag={country.flag}/>
    </>
  )
}

export default Country