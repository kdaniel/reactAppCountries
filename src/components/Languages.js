import React from 'react'
import Language from './Language';

const Languages = ({languages }) => languages.map(language => <Language key={language.name} language={language.name}/>)

export default Languages