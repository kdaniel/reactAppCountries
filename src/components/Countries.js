import React from 'react'

  const Countries = ({countries}) => countries.map(country => <p key={country.name}>{country.name}</p>)


export default Countries