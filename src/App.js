import React, { useState, useEffect } from 'react';
import axios from 'axios'

import Countries from './components/Countries';
import Filter from './components/Filter';
import Country from './components/Country';


const App = () => {

  const [countries, setCountries] = useState([])
  const [findCountry, setFindCountry] = useState("")

  const toShow = () => findCountry === '' ? [] : countries.filter(country => country.name.toLowerCase().includes(findCountry.toLowerCase()))

  const handleCountrySearch = (event) => {
    const countryToFind = event.target.value
    setFindCountry(countryToFind)
  }


  useEffect(() => {
    axios.get('https://restcountries.eu/rest/v2/all').then(response => setCountries(response.data))
  }, [])


  return (
    <>
      <h1>Find Country</h1>
      <Filter value={findCountry} onChange={handleCountrySearch} />
      {findCountry === '' && countries.length > 10 ? 
        <p>search a country by typing it's name</p> :
        (toShow().length > 10 ?
          (<p>to many results, be more specific</p>) :
            (<Countries countries={toShow()} />))}
      
      {toShow().length === 1 && <Country country={toShow()[0]} />}
    </>
  )
}
export default App

